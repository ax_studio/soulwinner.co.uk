
UPDATE LOG

------------------
1.5.0
FIXED: Various small bugs
TESTED ON: WordPress 5.5.3 and WooCommerce 4.6.1

1.4.16
FIXED: Accent characters stripped out of custom fields
TESTED ON: WordPress 5.4.2 and WooCommerce 4.2.2

1.4.14
FIXED: Field checkout validation bug
TESTED ON: WordPress 5.4.2 and WooCommerce 4.2.0

1.4.12
FIXED: Field checkout validation bug
TESTED ON: WordPress 5.4.2 and WooCommerce 4.2.0

1.4.11
FIXED: Field data sync bug with large number of tickets ordered 
FIXED: Various small bugs
TESTED ON: WordPress 5.4.2 and WooCommerce 4.2.0

1.4.7
FIXED: Field validation bug
FIXED: Various small bugs

1.4.6
FIXED: Various small bugs

1.4.4
UPDATED: Event settings options and layout

1.4.3
FIXED: Option translations

1.4.2
FIXED: Bug where optional fields are still format validated

1.4.0
ADDED: Support for new settings page
FIXED: Various small bugs

1.3.1
FIXED: Version number error message bug

1.3.0
ADDED: New custom attendee field options 
ADDED: Default value selection for select and radio options
ADDED: Functionality to reorder custom attendee field options
FIXED: Various small bugs

1.2.12
FIXED: jQuery conflict with certain themes and plugins
FIXED: Various other small bugs

1.2.10
ADDED: Various compliance updates
FIXED: Various small bugs

1.2.9
FIXED: Label character count increased 
FIXED: Various bugs

1.2.7
FIXED: JavaScript bug causing issues with saving fields

1.2.6
UPDATED: Documentation
UPDATED: Plugin option clean-up on delete 
FIXED: Various small bugs
FIXED: Duplicate option bug 

1.2.4
FIXED: Terminology new site bug
FIXED: Various other small bugs

1.2.3
ADDED: Terminology override feature

1.2.2
FIXED: Update notification bug

1.1.10
FIXED: Place holder bug

1.1.9
FIXED: Custom attendee multiple event bug
FIXED: Select box bug
ADDED: Theme option to use placeholders

1.1.5
FIXED: 10+ attendee bug

1.1.2
FIXED: Update notification bug 

1.1.0
-FIXED: Wordpress 4.8 bugs

1.0.7
-FIXED: Fields are now longer
-FIXED: Updated translations
-FIXED: Various other small bugs

1.0.4

-FIXED: Required fields bug
-FIXED: Various other small bugs  

1.0.1

-FIXED: Update notification bug 

