<?php
/**
 * Plugin Name: FooEvents Custom Attendee Fields
 * Description: Capture custom attendee information on checkout
 * Version: 1.5.0
 * Author: FooEvents
 * Plugin URI: https://www.fooevents.com/
 * Author URI: https://www.fooevents.com/
 * Developer: FooEvents
 * Developer URI: https://www.fooevents.com/
 * Text Domain: fooevents-custom-attendee-fields
 *
 * Copyright: © 2009-2020 FooEvents.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

//include config
require(WP_PLUGIN_DIR.'/fooevents_custom_attendee_fields/config.php');

class Fooevents_Custom_Attendee_Fields {
    
    private $Config;
    private $UpdateHelper;
    
    public function __construct() {
        
        add_action('admin_notices', array($this, 'check_fooevents'));
        add_action('woocommerce_product_write_panel_tabs', array($this, 'add_product_custom_attendee_field_options_tab'), 24);
        add_action('woocommerce_product_data_panels', array($this, 'add_product_custom_attendee_field_options_tab_options'));
        add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
        add_action('admin_enqueue_scripts', array($this, 'register_styles'));
        add_action('woocommerce_process_product_meta', array($this, 'process_meta_box'));
        add_action('plugins_loaded', array($this, 'load_text_domain'));
        add_action('save_post', array(&$this, 'save_ticket_meta_boxes'), 1, 2);
        add_filter('woocommerce_form_field_fooeventshidden', array($this, 'create_checkout_hidden_field'), 999, 4);
        
        add_action('wp_ajax_fooevents_fetch_add_ticket_attendee_options', array($this, 'fetch_add_ticket_attendee_options'));
        
        $this->plugin_init();
        
    }
    
    /**
     * Initializes plugin
     * 
     */
    public function plugin_init() {
        
        //Main config
        $this->Config = new FooEvents_Custom_Attendee_Fields_Config();
        
        //UpdateHelper
        require_once($this->Config->classPath.'updatehelper.php');
        $this->UpdateHelper = new Fooevents_Custom_Attendee_Fields_Update_Helper($this->Config);
        
    }
    
    /**
     * Initializes the WooCommerce meta box
     * 
     */
    public function add_product_custom_attendee_field_options_tab() {

        echo '<li class="custom_tab_custom_attendee_options"><a href="#fooevents_custom_attendee_field_options">'.__( 'Custom Attendee Fields', 'fooevents-custom-attendee-fields' ).'</a></li>';

    }
    
    /**
     * Add custom attendee field tabs 
     * 
     */
    public function add_product_custom_attendee_field_options_tab_options() {
        
        global $post;
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($post->ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);

        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        if(empty($fooevents_custom_attendee_fields_options)) {
            
            $fooevents_custom_attendee_fields_options = array();
            
        }

        require($this->Config->templatePath.'custom_attendee_fields_options.php');
        
    }
    
    /**
     * Processes the meta box form once the plubish / update button is clicked.
     * 
     * @global object $woocommerce_errors
     * @param int $post_id
     */
    public function process_meta_box($post_id) {
        
        global $woocommerce_errors;

        if(isset($_POST['fooevents_custom_attendee_fields_options_serialized'])) {
            
            $var = $_POST['fooevents_custom_attendee_fields_options_serialized'];
            
            $fooevents_custom_attendee_fields_options_serialized = $_POST['fooevents_custom_attendee_fields_options_serialized'];
            update_post_meta($post_id, 'fooevents_custom_attendee_fields_options_serialized', $fooevents_custom_attendee_fields_options_serialized);

        }
        
    }
    
    /**
     * Register plugin scripts.
     * 
     */
    public function register_scripts() {
        
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('fooevents-custom-attendee-fields-script',  $this->Config->scriptsPath . 'attendee-custom-fields.js', array('jquery', 'jquery-ui-sortable'), '1.2.0', true);
        
        if(isset($_GET['post_type']) && $_GET['post_type'] == 'event_magic_tickets') {
            
            wp_enqueue_script('events-attendee-fields-admin-add-ticket', $this->Config->scriptsPath . 'events-attendee-fields-admin-add-ticket.js', array('jquery'), '1.2.0', true);
            wp_localize_script('events-attendee-fields-admin-add-ticket', 'FooEventsAttendeeAddTicketObj', array('ajaxurl' => admin_url('admin-ajax.php')));
            
        }
        
    }
    
    /**
     * Register plugin styles.
     * 
     */
    public function register_styles() {
        
        wp_enqueue_style('fooevents-custom-attendee-fields-style', $this->Config->stylesPath.'attendee-custom-fields.css', array(), '1.0.0');

    }
    
    /**
     * Checks if FooEvents is installed
     * 
     */
    public function check_fooevents() {
        
        $fooevents_path = WP_PLUGIN_DIR.'/fooevents/fooevents.php';

        $plugin_data = get_plugin_data($fooevents_path);

        if (!is_plugin_active('fooevents/fooevents.php')) {

            $this->output_notices(array(__('The FooEvents Custom Attendee plugin requires FooEvents for WooCommerce to be installed.', 'fooevents-custom-attendee-fields')));

        } 
        
    }
    
    /**
     * Outputs custom attendee fields on the checkout screen
     * 
     * @param int $product_ID
     * @param int $x
     * @param int $y
     * @param array $ticket
     * @param object $checkout
     */
    public function output_attendee_fields($product_ID, $x, $y, $ticket, $checkout) {
        
        global $woocommerce;
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);

        $attendeeTerm = get_post_meta($ticket['product_id'], 'WooCommerceEventsAttendeeOverride', true);

        if(empty($attendeeTerm)) {

            $attendeeTerm = get_option('globalWooCommerceEventsAttendeeOverride', true);

        }
        
        if(empty($attendeeTerm) || $attendeeTerm == 1) {

            $attendeeTerm = __('Attendee', 'fooevents-custom-attendee-fields');

        }

        if(!empty($fooevents_custom_attendee_fields_options)) {
            
            $z = 1;
            
            foreach($fooevents_custom_attendee_fields_options as $option_key => $option) {

                $required = ($option[$option_key.'_req'] == 'true')? true : false;
                $option_label_output = sprintf(__('%s', 'fooevents-custom-attendee-fields'), ucwords($option[$option_key.'_label']));
                $textLabel = $option_label_output;
                $option_label_output_encoded = $this->_encode_custom_field_name($option[$option_key.'_label']);
                $globalWooCommerceUsePlaceHolders = get_option('globalWooCommerceUsePlaceHolders', true);
                $fieldParams = array();
                $fieldParams2 = array();
                $value = '';
                
                if($option[$option_key.'_type'] == 'text') {

                    $fieldParams = array(
                        'type'          => 'text',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                  
                }  elseif($option[$option_key.'_type'] == 'textarea') {

                    $fieldParams = array(
                        'type'          => 'textarea',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders == 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
     
                } elseif($option[$option_key.'_type'] == 'select') {

                    $select_values = array();
                    if(!empty($option[$option_key.'_options'])) {
                        
                        $select_options = explode('|', $option[$option_key.'_options']);

                        foreach($select_options as $select_option) {
                            
                            $select_option = trim($select_option);
                            $select_values[$select_option] = $select_option;
                            
                        }
                        
                    }

                    if(!empty($option[$option_key.'_def'])) {

                        $select_values = array_merge(array("" => $option[$option_key.'_def']), $select_values); 
                        
                    }

                    $fieldParams = array(
                        'type'          => 'select',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'options'       => $select_values,
                        'required'      => $required
                    );

                    if($globalWooCommerceUsePlaceHolders == 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                
                }elseif($option[$option_key.'_type'] == 'checkbox') {

                    $fieldParams = array(
                        'type'          => 'checkbox',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );
                    
                    $fieldParams2 = array(
                        'type'          => 'fooeventshidden',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'radio') {
                    
                    $select_values = array();
                    if(!empty($option[$option_key.'_options'])) {
                        
                        $select_options = explode('|', $option[$option_key.'_options']);

                        foreach($select_options as $select_option) {
                            
                            $select_option = trim($select_option);
                            $select_values[$select_option] = $select_option;
                            
                        }
                        
                    }

                    if(!empty($option[$option_key.'_def'])) {

                        $select_values = array_merge(array("" => $option[$option_key.'_def']), $select_values); 
                        
                    }
                    
                    $fieldParams = array(
                        'type'          => 'radio',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'options'       => $select_values,
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'country') {

                    $fieldParams = array(
                        'type'          => 'country',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'date') {

                    $fieldParams = array(
                        'type'          => 'date',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'time') {

                    $fieldParams = array(
                        'type'          => 'time',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'email') {
                    
                    $fieldParams = array(
                        'type'          => 'text',
                        'class'         => array('attendee-class', 'form-row-wide', 'fooevents-email'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'url') {

                    $fieldParams = array(
                        'type'          => 'text',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'fooevents-val' => 'fooevents-url',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'numbers') {

                    $fieldParams = array(
                        'type'          => 'number',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'fooevents-val' => 'fooevents-numbers',
                        'required'      => $required,    
                    );
                    
                    $value = 0;
                    
                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'alphabet') {

                    $fieldParams = array(
                        'type'          => 'text',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'fooevents-val' => 'fooevents-alphabet',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                } elseif($option[$option_key.'_type'] == 'alphanumeric') {

                    $fieldParams = array(
                        'type'          => 'text',
                        'class'         => array('attendee-class form-row-wide'),
                        'label'         => $option_label_output,
                        'placeholder'   => '',
                        'fooevents-val' => 'fooevents-alphanumeric',
                        'required'      => $required,    
                    );

                    if($globalWooCommerceUsePlaceHolders === 'yes') {

                        $fieldParams['placeholder'] = $textLabel;

                    }
                   
                }
                
                if(!empty($fieldParams2)) {

                    woocommerce_form_field("fooevents_custom_".$option_key.'_'.$x.'__'.$y, $fieldParams2, $checkout->get_value("fooevents_custom_".$option_key.'_'.$x.'__'.$y ));

                }  
                
                if($checkout->get_value("fooevents_custom_".$option_key.'_'.$x.'__'.$y )) {
                    
                    $value = $checkout->get_value("fooevents_custom_".$option_key.'_'.$x.'__'.$y );
                    
                }
                
                if(!empty($fieldParams)) {

                    woocommerce_form_field("fooevents_custom_".$option_key.'_'.$x.'__'.$y, $fieldParams, $value);

                }    
                
                $z++;

            }
            
        }
        
    }
    
    /**
     * Filter to create hidden fields on checkout
     * 
     */
    public function create_checkout_hidden_field($no_parameter, $key, $args, $value) {
        
        
        if(isset($args['id']) && strpos($args['id'], 'fooevents_custom_') === 0) {

            $field = '<p class="form-row ' . implode( ' ', $args['class'] ) .'" id="' . $key . '_field">
            <input type="hidden" class="input-hidden" name="' . $key . '" id="' . $key . '" placeholder="' . $args['placeholder'] . '" value="0" />
            </p>';

            return $field;
        }

    }
    
    /**
     * Validate bookings selection on add ticket page
     * 
     */
    public function admin_add_ticket_custom_fields_validate($event_id, $fields) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($event_id,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        foreach($fields as $key => $value) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);

                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                } elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }
                
                if(isset($fooevents_custom_attendee_fields_options[$field_id]) && $fooevents_custom_attendee_fields_options[$field_id][$field_id.'_req'] == true) {
                    
                    if(empty($fields[$key])) {
                        
                        $message = sprintf(__('%s is a required field.', 'fooevents-custom-attendee-fields'), $fooevents_custom_attendee_fields_options[$field_id][$field_id.'_label']);
                        return json_encode(array("type" => "error", "message" => $message)); 
                        
                    }
                    
                }
               
           }
            
        }

    }
    
    /**
     * Validate bookings selection on add ticket page
     * 
     */
    public function admin_edit_ticket_custom_fields_validate($event_id, $fields) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($event_id,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);

        foreach($fields as $key => $value) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);

                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                } elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }
                
                if(isset($fooevents_custom_attendee_fields_options[$field_id]) && $fooevents_custom_attendee_fields_options[$field_id][$field_id.'_req'] == true) {
                    
                    if(empty($fields[$key])) {
                        
                        $message = sprintf(__('%s is a required field.', 'fooevents-custom-attendee-fields'), $fooevents_custom_attendee_fields_options[$field_id][$field_id.'_label']);
                        return json_encode(array("type" => "error", "message" => $message)); 
                        
                    }
                    
                }
               
           }
            
        }

    }
    
    public function fetch_attendee_details_for_order($product_ID, $detected_custom_fields) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        $custom_values = array();
        
        foreach($detected_custom_fields as $key => $value) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);

                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                } elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }

                if(isset($fooevents_custom_attendee_fields_options[$field_id])) {
                    
                    $custom_values[$field_id]['name'] = $key;
                    $custom_values[$field_id]['value'] = esc_attr($value);
                    $custom_values[$field_id]['field'] = $fooevents_custom_attendee_fields_options[$field_id];
               
                }
               
           }
            
        }
        
        return $custom_values;
        
    }
    
    public function fetch_attendee_details_for_order_generated($product_ID, $ticket_ID) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        $custom_values = array();
        
        $post_meta = get_post_meta($ticket_ID); 
        
        foreach($post_meta as $key => $meta) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);

                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                } elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }

                if(isset($fooevents_custom_attendee_fields_options[$field_id])) {
                    
                    $custom_values[$field_id]['name'] = $key;
                    $custom_values[$field_id]['value'] = esc_attr($meta[0]);
                    $custom_values[$field_id]['field'] = $fooevents_custom_attendee_fields_options[$field_id];
               
                }
               
           }
            
        }

        return $custom_values;
        
    }

    public function validate_custom_fields($ticket, $event, $x, $y) {
        
        global $woocommerce;
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($ticket['product_id'],'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        if(!empty($fooevents_custom_attendee_fields_options)) {
            
            $z = 1;
            
            foreach($fooevents_custom_attendee_fields_options as $option_key => $option) {
                
                $label = $option[$option_key.'_label'];

                $field_id = "fooevents_custom_".$option_key.'_'.$x.'__'.$y;

                if($option[$option_key.'_req'] == 'true') {
                     
                    if (empty($_POST[$field_id])) {

                        $notice = sprintf( ucfirst($option[$option_key.'_label']) . __(' is required for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y );
                        wc_add_notice( $notice, 'error' );
                        continue;
                    }
                    
                } 
                
                if($option[$option_key.'_type'] == 'email') {
 
                    if (!$this->is_email_valid($_POST[$field_id])) {

                        $notice = sprintf(__( ucfirst($option[$option_key.'_label']).' is not a valid email address for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y);
                        wc_add_notice( $notice, 'error' );

                    }
                    
                } elseif($option[$option_key.'_type'] == 'url') {
                    
                    if (!filter_var($_POST[$field_id], FILTER_VALIDATE_URL)) {
                        
                        $notice = sprintf(__( ucfirst($option[$option_key.'_label']).' is not a valid URL for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y);
                        wc_add_notice( $notice, 'error' );
                        
                    }
                    
                } elseif($option[$option_key.'_type'] == 'numbers') {
                    
                    if (!filter_var($_POST[$field_id], FILTER_VALIDATE_INT)) {
                        
                        $notice = sprintf(__( ucfirst($option[$option_key.'_label']).' is not a valid number for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y);
                        wc_add_notice( $notice, 'error' );
                        
                    }
                    
                } elseif($option[$option_key.'_type'] == 'alphabet') {
                    
                    if (preg_match('/[^A-Za-z]/', $_POST[$field_id])) {
                        
                        $notice = sprintf(__( ucfirst($option[$option_key.'_label']).' needs to contain only alphabet characters for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y);
                        wc_add_notice( $notice, 'error' );
                        
                    }
                    
                } elseif($option[$option_key.'_type'] == 'alphanumeric') {
                    
                    if (preg_match('/[^A-Za-z0-9]/', $_POST[$field_id])) {
                        
                        $notice = sprintf(__( ucfirst($option[$option_key.'_label']).' needs to contain only alphabet or number characters for %s attendee %d', 'fooevents-custom-attendee-fields' ), $event, $y);
                        wc_add_notice( $notice, 'error' );
                        
                    }
                    
                }
                
                $z++;
                
            }
            
        }

    }
    
    /**
     * Display custom attendee fields on tickets.
     * 
     * @param int $ticket_id
     * @param int $product_id 
     * @return array
     */
    public function display_tickets_meta_custom_options_output($ticket_id, $product_id) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_id,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        $post_meta = get_post_meta($ticket_id);
        $custom_values = array();

        foreach($post_meta as $key => $meta) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);
                
                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                }
                //LEGACY: 20200720
                elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }
                //ENDLEGACY: 20200720

                if(isset($fooevents_custom_attendee_fields_options[$field_id])) {
                    
                    $custom_values[$field_id]['name'] = $key;
                    $custom_values[$field_id]['value'] = esc_attr($meta[0]);
                    $custom_values[$field_id]['label'] = $fooevents_custom_attendee_fields_options[$field_id][$field_id.'_label'];
               
                }
               
           }
            
        }

        return $custom_values;
        
    }
    
    /**
     * Display custom attendee fields on tickets.
     * 
     * @param int $ID
     * @return string
     */
    //LEGACY: 20200831
    public function display_tickets_meta_custom_options_output_legacy($ticket_id, $product_id) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_id,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        $post_meta = get_post_meta($ticket_id); 
        $output = '';
        
        foreach($post_meta as $key => $meta) {
            
           if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);
                
                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                }
                //LEGACY: 20200831
                elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }
                //ENDLEGACY: 20200831

                if(isset($fooevents_custom_attendee_fields_options[$field_id])) {

                    $custom_values[$key] = esc_attr($meta[0]);
                    $output .= '<tr><td width="160px" valign="top">'.$fooevents_custom_attendee_fields_options[$field_id][$field_id.'_label'].':</td><td valign="top">'.esc_attr($meta[0])."</td></tr>";
                    
                }

           }
            
        }
        $output = '<table border="0" cellspacing="0" cellpadding="3" width="100%">'.$output.'</table>';
        return $output;
        
    }
    //ENDLEGACY: 20200831
    
    /**
     * Displays include custom attendee options 
     * 
     * @param object $post
     * @return string
     */
    public function generate_include_custom_attendee_options($post) {
        
        ob_start();
        
        $WooCommerceEventsIncludeCustomAttendeeDetails = get_post_meta($post->ID, 'WooCommerceEventsIncludeCustomAttendeeDetails', true);

        require($this->Config->templatePath.'product_custom_attendee_options.php');

        $custom_attendee_options = ob_get_clean();
        
        return $custom_attendee_options;
        
    }
    
    /**
     * Formats custom attendee fields in array
     * 
     * @param int $ID
     * @return array
     */
    public function display_tickets_meta_custom_options_array($ID) {
        
        $post_meta = get_post_meta($ID); 
        $output = array();
        
        foreach($post_meta as $key => $meta) {
            
           if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = strtolower($key);
                $custom_values[$field] = $meta[0];
                $output[$field] = $meta[0];
                
           }
            
        }
        
        return $output;
        
    }
    
    /**
     * Formats custom attendee fields for CSV
     * 
     * @param int $ID
     * @return array
     */
    public function display_tickets_meta_custom_options_array_csv($ID, $product_ID) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        
        $post_meta = get_post_meta($ID); 
        $custom_values = array();
        
        foreach($post_meta as $key => $meta) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);
                
                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                }
                //LEGACY: 20200720
                elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }
                //ENDLEGACY: 20200720

                if(isset($fooevents_custom_attendee_fields_options[$field_id])) {
                    
                    $custom_values[$fooevents_custom_attendee_fields_options[$field_id][$field_id.'_label']] = esc_attr($meta[0]);
               
                }
               
           }
            
        }

        return $custom_values;
        
    }
    
    /**
     * Captures custom attendee options on checkout.
     * 
     * @param int $product_id
     * @param int $x
     * @param int $y
     */
    public function capture_custom_attendee_options($product_id, $x, $y) {

        $custom_values = array();

        foreach($_POST as $key => $value) {

            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_id,'fooevents_custom_attendee_fields_options_serialized', true);
                $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
                $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
                
                $field = explode('_', $key);
                $field = array_reverse($field);

                if($field[2] == $x && $field[0] == $y) {
                    
                    //LEGACY: 20200720
                    if($field[3] == 'option') {
                        
                        $field[3] = $field[3].'_'.$field[4];
                        
                    }
                    //ENDLEGACY: 20200720
                    
                    $field_name = $fooevents_custom_attendee_fields_options[$field[3]][$field[3].'_label'];
                    $custom_values['fooevents_custom_'.$field[3]] = sanitize_text_field($value);

                }

            }
            
        }

        return $custom_values;
        
    }

    /**
     * Capture custom attendee options
     * 
     * @param int $postID
     * @param array $WooCommerceEventsCustomAttendeeFields
     * 
     */
    public function process_capture_custom_attendee_options($postID, $WooCommerceEventsCustomAttendeeFields) {

        foreach($WooCommerceEventsCustomAttendeeFields as $key => $value) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {

                $key = esc_attr($key);
                $value = esc_attr($value);
                update_post_meta($postID, $key, $value);

            }
            
        }
        
    }
    
    /**
     * Outputs custom attendee options in admin
     * 
     * @param int $ticket_post_ID
     * @param int $product_ID
     * @return string
     * 
     */
    public function ticket_details_attendee_fields($ticket_post_ID, $product_ID) {
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($product_ID,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);

        $post_meta = get_post_meta($ticket_post_ID);
        $custom_values = array();

        foreach($post_meta as $key => $meta) {
            
            if (strpos($key, 'fooevents_custom_') === 0) {
                
                $field = explode('_', $key);
                
                $field_id = '';
                if(isset($fooevents_custom_attendee_fields_options[$field[2]])) {
                    
                    $field_id = $field[2];
                    
                }
                //LEGACY: 20200720
                elseif(isset($fooevents_custom_attendee_fields_options[$field[3].'_option'])) {
                    
                    $field_id = $field[3].'_option';
                    
                }
                //ENDLEGACY: 20200720

                if(isset($fooevents_custom_attendee_fields_options[$field_id])) {
                    
                    $custom_values[$field_id]['name'] = $key;
                    $custom_values[$field_id]['value'] = esc_attr($meta[0]);
                    $custom_values[$field_id]['field'] = $fooevents_custom_attendee_fields_options[$field_id];
               
                }
               
           }
            
        }

        ob_start();

        require($this->Config->templatePath.'custom_attendee_ticket_detail.php'); 

        $custom_options = ob_get_clean();

        return $custom_options;
        
    }
    
    /**
     * Saves tickets meta box settings
     * 
     * @param int $post_ID
     * @global object $post
     * @global object $woocommerce
     * 
     */
    public function save_ticket_meta_boxes($post_ID) {
        
        global $post;
        global $woocommerce;
        
        if (is_object($post) && isset($_POST)) {

            if($post->post_type == "event_magic_tickets") {
                
                foreach($_POST as $key => $value) {
                    
                    if (strpos($key, 'fooevents_custom_') === 0) {
                        
                        $value = sanitize_text_field($value);
                        update_post_meta($post_ID, $key, $value);
 
                    }
                    
                }
                
            }
            
        }
        
    }
    
    /**
     * Loads text-domain for localization
     * 
     */
    public function load_text_domain() {

        $path = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
        $loaded = load_plugin_textdomain( 'fooevents-custom-attendee-fields', false, $path);
        
    }
    
    /**
     * Fetch booking options for add ticket page
     * 
     */
    public function fetch_add_ticket_attendee_options() {

        $event_id = sanitize_text_field($_POST['event_id']);
        
        $fooevents_custom_attendee_fields_options_serialized = get_post_meta($event_id,'fooevents_custom_attendee_fields_options_serialized', true);
        $fooevents_custom_attendee_fields_options = json_decode($fooevents_custom_attendee_fields_options_serialized, true);
        $fooevents_custom_attendee_fields_options = $this->correct_legacy_options($fooevents_custom_attendee_fields_options);
        $custom_values = array();
        
        foreach($fooevents_custom_attendee_fields_options as $key => $field) {

            $custom_values[$key]['name'] = 'fooevents_custom_'.$key;
            $custom_values[$key]['value'] = '';
            $custom_values[$key]['field'] = $field;

        }

        require_once($this->Config->templatePath.'custom_attendee_fields_add_ticket.php');
        
        exit();
        
    }
    
    /**
     * Converts legacy custom attendee field options to new format
     * 
     * @param array $fooevents_custom_attendee_fields_options
     * @return array $fooevents_custom_attendee_fields_options  
     */
    public function correct_legacy_options($fooevents_custom_attendee_fields_options) {

        $processed_fooevents_custom_attendee_fields_options = array();
        
        if(!empty($fooevents_custom_attendee_fields_options)) {

            $x = 1;
            foreach($fooevents_custom_attendee_fields_options as $option_key => $option) {
                
                /*echo "<pre>";
                    print_r($option);
                echo "</pre>";*/
                
                if (strpos($option_key, 'option') !== false) {
                    
                    if(isset($option[$x.'_label'])) {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key][$option_key.'_label'] = $option[$x.'_label'];
                        
                    } else {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key] = $option;
                        
                    }
                    
                    if(isset($option[$x.'_type'])) {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key][$option_key.'_type'] = $option[$x.'_type'];
                    
                    } else {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key] = $option;
                        
                    }
                    
                    if(isset($option[$x.'_options'])) {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key][$option_key.'_options'] = $option[$x.'_options'];
                        
                    } else {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key] = $option;
                        
                    }
                    
                    if(isset($option[$x.'_def'])) {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key][$option_key.'_def'] = $option[$x.'_def'];
                        
                    } else {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key] = $option;
                        
                    }
                    
                    if(isset($option[$x.'_req'])) {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key][$option_key.'_req'] = $option[$x.'_req'];
                        
                    } else {
                        
                        $processed_fooevents_custom_attendee_fields_options[$option_key] = $option;
                        
                    }

                    $x++;

                } else {

                    $processed_fooevents_custom_attendee_fields_options[$option_key] = $option;

                }

            }
        
        }
        
        return $processed_fooevents_custom_attendee_fields_options;

    }

    private function _process_label($label) {
        
        
        return preg_replace('/[^A-Za-z0-9\-]/', '_', $label);
        
    }
    
    /**
     * Checks a string for valid email address
     * 
     * @param string $email
     * @return bool
     */
    private function is_email_valid($email) {
        
        return filter_var($email, FILTER_VALIDATE_EMAIL) 
            && preg_match('/@.+\./', $email);
        
    }
    
    /**
     * Formats field name
     * 
     * @param string $field_name
     * @return string
     */
    private function _output_custom_field_name($field_name) {
        
        $field_name = str_replace('fooevents_custom_', "", $field_name);
        $field_name = str_replace('_', " ", $field_name);
        $field_name = ucwords($field_name);
        
        return $field_name;
        
    }
    
    /**
     * Formats field name
     * 
     * @param string $field_name
     * @return string
     */
    private function _encode_custom_field_name($field_name) {
        
        $remove = array(',', '!', '?', "'", '.');
        $field_name = str_replace(' ', '_', $field_name);
        $field_name = str_replace($remove, '', $field_name); 
        $field_name = esc_attr($field_name);
 
        return $field_name;
        
    }
    
    /**
     * Outputs notices to screen.
     * 
     * @param array $notices
     */
    private function output_notices($notices) {

        foreach ($notices as $notice) {

            echo "<div class='updated'><p>$notice</p></div>";

        }

    }
    
}

$Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();