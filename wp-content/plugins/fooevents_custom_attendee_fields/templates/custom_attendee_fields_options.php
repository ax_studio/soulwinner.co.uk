<div id="fooevents_custom_attendee_field_options" class="panel woocommerce_options_panel" style="display: block;">
    <p><h2><b><?php _e('Custom Attendee Fields', 'woocommerce-events'); ?></b></h2></p>
    <div class="fooevents_custom_attendee_fields_wrap">
        <div class="options_group">
            <table id="fooevents_custom_attendee_fields_options_table" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th></th>
                        <th><?php echo __( 'Label', 'fooevents-custom-attendee-fields' ); ?></th>
                        <th><?php echo __( 'Type', 'fooevents-custom-attendee-fields' ); ?></th>
                        <th><?php echo __( 'Options', 'fooevents-custom-attendee-fields' ); ?></th>
                        <th><?php echo __( 'Default', 'fooevents-custom-attendee-fields' ); ?></th>
                        <th><?php echo __( 'Required', 'fooevents-custom-attendee-fields' ); ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $x = 0; $y = 1; ?>
                    <?php foreach($fooevents_custom_attendee_fields_options as $option_key => $option) :?>
                    <?php $option_ids = array_keys($option); ?>
                    <?php $option_values = array_values($option); ?>
                    <?php $num_option_ids = count($option_ids); $num_option_values = count($option_values); ?>
                    <?php if($num_option_ids == $num_option_values): ?>
                    <tr id="<?php echo $option_key; ?>">
                        <td>
                            <span class="dashicons dashicons-menu"></span>
                        </td>
                        <td>
                            <input type="text" id="<?php echo $option_key; ?>_label" name="<?php echo $option_key; ?>_label" class="fooevents_custom_attendee_fields_label" value="<?php echo $option[$option_key.'_label']; ?>" autocomplete="off" maxlength="150"/>
                        </td>
                        <td>
                            <select id="<?php echo $option_key; ?>_type" name="<?php echo $option_key; ?>_type" class="fooevents_custom_attendee_fields_type" autocomplete="off">
                                <option value="text" <?php echo ($option[$option_key.'_type'] == 'text')? 'SELECTED' : ''; ?>><?php echo __( 'Text', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="textarea" <?php echo ($option[$option_key.'_type'] == 'textarea')? 'SELECTED' : ''; ?>><?php echo __( 'Textarea', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="select" <?php echo ($option[$option_key.'_type'] == 'select')? 'SELECTED' : ''; ?>><?php echo __( 'Select', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="checkbox" <?php echo ($option[$option_key.'_type'] == 'checkbox')? 'SELECTED' : ''; ?>><?php echo __( 'Checkbox', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="radio" <?php echo ($option[$option_key.'_type'] == 'radio')? 'SELECTED' : ''; ?>><?php echo __( 'Radio', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="country" <?php echo ($option[$option_key.'_type'] == 'country')? 'SELECTED' : ''; ?>><?php echo __( 'Country', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="date" <?php echo ($option[$option_key.'_type'] == 'date')? 'SELECTED' : ''; ?>><?php echo __( 'Date', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="time" <?php echo ($option[$option_key.'_type'] == 'time')? 'SELECTED' : ''; ?>><?php echo __( 'Time', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="email" <?php echo ($option[$option_key.'_type'] == 'email')? 'SELECTED' : ''; ?>><?php echo __( 'Email', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="url" <?php echo ($option[$option_key.'_type'] == 'url')? 'SELECTED' : ''; ?>><?php echo __( 'URL', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="numbers" <?php echo ($option[$option_key.'_type'] == 'numbers')? 'SELECTED' : ''; ?>><?php echo __( 'Numbers', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="alphabet" <?php echo ($option[$option_key.'_type'] == 'alphabet')? 'SELECTED' : ''; ?>><?php echo __( 'Alphabet', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="alphanumeric" <?php echo ($option[$option_key.'_type'] == 'alphanumeric')? 'SELECTED' : ''; ?>><?php echo __( 'Alphanumeric', 'fooevents-custom-attendee-fields' ); ?></option>
                            </select>
                        </td>
                        <td>
                            <input id="<?php echo $option_key; ?>_options" name="<?php echo $option_key; ?>_options" class="fooevents_custom_attendee_fields_options" type="text" value="<?php echo $option[$option_key.'_options']; ?>" <?php echo ($option[$option_key.'_type'] == 'select' || $option[$option_key.'_type'] == 'radio')? '' : 'disabled'; ?> autocomplete="off" />    
                        </td>
                        <td>
                            <input id="<?php echo $option_key; ?>_def" name="<?php echo $option_key; ?>_def" class="fooevents_custom_attendee_fields_def" type="text" value="<?php echo (!empty($option[$option_key.'_def'])) ? $option[$option_key.'_def'] : "";?>" <?php echo ($option[$option_key.'_type'] == 'select' || $option[$option_key.'_type'] == 'radio')? '' : 'disabled'; ?> autocomplete="off" />    
                        </td>
                        <td>
                            <select id="<?php echo $option_key; ?>_req" name="<?php echo $option_key; ?>_req" class="fooevents_custom_attendee_fields_req" autocomplete="off">
                                <option value="true" <?php echo ($option[$option_key.'_req'] == 'true')? 'SELECTED' : ''; ?>><?php echo __( 'Yes', 'fooevents-custom-attendee-fields' ); ?></option>
                                <option value="false" <?php echo ($option[$option_key.'_req'] == 'false')? 'SELECTED' : ''; ?>><?php echo __( 'No', 'fooevents-custom-attendee-fields' ); ?></option>
                            </select>
                        </td>
                        <td><a href="#" id="<?php echo $option_key; ?>_remove" name="<?php echo $option_key; ?>_remove" class="fooevents_custom_attendee_fields_remove" class="fooevents_custom_attendee_fields_remove">[X]</a></td>
                    </tr>
                    <?php $x++; $y++; ?>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>    
        </div>
    </div>
    <div id="fooevents_custom_attendee_fields_info">
        <p><a href="#" id="fooevents_custom_attendee_fields_new_field" class='button button-primary'>+ <?php echo __( 'New field', 'fooevents-custom-attendee-fields' ); ?></a></p>
        <p class="description"><?php echo __( "When using the 'Select' or 'Radio' options, seperate the options using a pipe symbol. Example: Small|Medium|Large.", 'fooevents-custom-attendee-fields' ); ?></p>
    </div>
    <input type="hidden" id="fooevents_custom_attendee_fields_options_serialized" name="fooevents_custom_attendee_fields_options_serialized" value="<?php echo $fooevents_custom_attendee_fields_options_serialized; ?>" autocomplete="off" />
</div>