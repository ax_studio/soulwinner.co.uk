<?php if(!empty($custom_values)): ?>
    <h3><?php _e('Custom Attendee Fields', 'fooevents-custom-attendee-fields'); ?></h3>
    <?php foreach($custom_values as $key => $field): ?>
    <?php $options = explode('|', $field['field'][$key.'_options']); ?>
    <div class="ticket-details-row">
        <label><?php echo $field['field'][$key.'_label']; ?>:</label>
        <?php $x =0; ?>
        <?php if($field['field'][$key.'_type'] == 'radio') :?> 
            <?php $x =0; ?>
            <?php foreach($options as $option) : $x ++;?>
            <label for="<?php echo $key.'_'.$x; ?>">
                <input type="radio" id="<?php echo $key.'_'.$x; ?>" name="<?php echo $field['name']; ?>" value="<?php echo $option; ?>" <?php echo ($field['value'] == $option)? 'CHECKED' : ''; ?> >
                <?php echo $option; ?>
            </label>
            <?php endforeach; ?>
        <?php elseif($field['field'][$key.'_type'] == 'select') :?> 
            <select name="<?php echo $field['name']; ?>">
                <?php foreach($options as $option) :?>
                <option value="<?php echo $option; ?>" <?php echo ($field['value'] == $option)? 'SELECTED' : ''; ?>><?php echo $option; ?></option>
                <?php endforeach; ?>
            </select>
        <?php elseif($field['field'][$key.'_type'] == 'checkbox') :?> 
        <input type="hidden" id="<?php echo $key.'_'.$x; ?>" name="<?php echo $field['name']; ?>" value="0">
        <input type="checkbox" id="<?php echo $key.'_'.$x; ?>" name="<?php echo $field['name']; ?>" value="1" <?php echo ($field['value'] == 1)? 'CHECKED' : ''; ?> >
        <?php else:?> 
            <input type="text" name="<?php echo $field['name']; ?>" value="<?php echo $field['value']; ?>" />
        <?php endif; ?>
    </div>
    <?php endforeach ?>
<?php endif; ?>
<div class="clear"></div>