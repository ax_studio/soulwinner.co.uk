<h2><?php _e( 'Custom Attendee Fields', 'fooevents-custom-attendee-fields' ); ?></h2>
<table class="form-table">
    <tbody>
        <?php foreach($custom_values as $key => $value) :?>
        <tr valign="top">  
            <td style="width: 200px;">
                <label><?php echo $this->_output_custom_field_name($key); ?>:</label><Br />
            </td>
            <td>
               <?php echo esc_attr($value); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>