<?php

class NJT_CUSTOMER_CHAT_API
{
    private $ver = 'v2.11';
    public function __construct()
    {
        
    }
    
    // Information admin page
    public function Me($token)
    {
        $me = $this->cURL(sprintf('https://graph.facebook.com/%1$s/me?access_token=%2$s&fields=id%%2Cname%%2Caccounts%%2Cpicture%%7Burl%%7D', $this->ver, $token));
        return $me;
    }

    // Get Page
    public function Get_List_Page($token)
    {
        $json = $this->cURL(sprintf('https://graph.facebook.com/%1$s/me/accounts?access_token=%2$s', $this->ver, $token));
        return isset($json->data) ? $json->data : array();
    }

    // SET DOMAIN
    public function addWhitelistDomain($id_page, $domain, $page_access_token)
    {
        $data = [
            'setting_type' => 'domain_whitelisting',
            'whitelisted_domains' => $domain,
            'domain_action_type' => 'add',
            'access_token' => $page_access_token
        ];
        $str = $this->cURL(sprintf('https://graph.facebook.com/%1$s/%2$s/thread_settings', $this->ver, $id_page), $data);
        
    }

    public function cURL($url, $post = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if(!is_null($post)) {
            $post = json_encode($post);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($post),
                )
            );
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($ch);
        curl_close($ch);

        return json_decode($return);
    }
}
