��    :      �      �      �     �  3   �  o    
   s     ~     �     �     �  	   �     �     �     �       %   (     N  �   T  
   �     �     �          &     -     <     U     h     {     �     �  :   �     �  
   �     �     �               2     @     I     [     q     }  =   �  
   �  4   �  =   	  &   K	  .   r	  -   �	  3   �	  ,   
     0
  0   D
     u
  N   y
  H   �
       %   ,     R     S  \   o  �  �     �     �     �     �  "        .  ,   J  *   w  6   �  \   �  ?   6     v  �   �  $   0  7   U  $   �  9   �     �     �  :     2   T     �  
   �     �  +   �  ;   �     3  
   M     X     _     s  (   �     �     �  5   �  H        ]  8   m  w   �       5   *  G   `  8   �  @   �  S   "  i   v  M   �     .  8   L     �  �   �  �   ,     �  &   �   Active WooInstant Actived window will open first when wooinstant open Are you tired of multi-step checkout process of WooCommerce? WooInstant is your solution. Install WooInstant and your multi-step checkout process will convert to One Page Checkout. Now, all your customer have to do is "Add to Cart", a popup will appear with the cart view. Your customer can then checkout and order from that single window!. No Page Reload whatsoever! BootPeople Cart Cart Toggler Background Cart Toggler Color Cart Toggler Hover Color Cart icon Checkout Choose Active Window Choose Cart Drawer Direction Choose Icon or Custom Image Choose how the window will looks like Close Control WooInstant z-index from this option. More about <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> Custom CSS Custom Image Default Icon Default is: Theme Default Design Design Options Disable Ajax Add to Cart Disable Quick View Drawer Window Type General General Options Hide Close Button? If you want to make extra CSS then you can do it from here Left to Right Multi Step No Panel Background Panel Z-index Quick View Background Right to Left Settings Show On Cart Page Show On Checkout Page Single Step Upload image/icon Upload your cart icon. Recommended size of an icon is 30x30px WooInstant WooInstant - WooCommerce Instant / One Page Checkout WooInstant - WooCommerce Instant / One Page Checkout Settings WooInstant Cart Panel Background Color WooInstant Cart Panel Toggler Background Color WooInstant Cart Panel Toggler Text/Icon Color WooInstant Cart Panel Toggler Text/Icon Hover Color WooInstant Quick View Panel Background Color WooInstant Settings Wooinstant requires WooCommerce to be activated  Yes You can disable it if you already have ajax add to cart function in your theme You can disable it if you already have quick view function in your theme https://psdtowpservice.com https://psdtowpservice.com/wooinstant Project-Id-Version: WooInstant - WooCommerce Instant / One Page Checkout
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-07-09 09:23+0000
PO-Revision-Date: 2020-07-25 07:13+0000
Last-Translator: 
Language-Team: Russian
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2 Активный WooInstant Активное окно откроется первым при открытии wooinstant Вы устали от многоэтапного оформления заказа в WooCommerce? WooInstant это ваше решение. Установите WooInstant, и процесс многоэтапного оформления заказа превратится в Одностраничный заказ. Теперь все, что нужно сделать вашему клиенту - это «Добавить в корзину», появится всплывающее окно с представлением корзины. Ваш клиент может оформить заказ и сделать заказ из этого единственного окна! Нет страницы Обновить вообще! BootPeople
 телега Корзина Toggler Фон Корзина Toggler Фон Корзина Toggler Hover Color Значок корзины Проверять, выписываться Выберите Активное окно Выберите направление корзины Выберите значок или пользовательское изображение Выберите, как будет выглядеть окно близко Управляйте WooInstant z-index из этой опции. Больше о <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> Пользовательские CSS Пользовательское изображение Значок по умолчанию По умолчанию: тема по умолчанию дизайн Варианты дизайна Отключить Ajax Добавить в корзину Отключить быстрый просмотр Тип окна ящика Общее Общие настройки Скрыть кнопку закрытия? If you want to make extra CSS then you can do it from here
 Слева направо Multi Step нет Фон панели Панель Z-index Быстрый просмотр фона Справа налево настройки Показать на странице корзины Показать на странице оформления заказа Один шаг Загрузить изображение / значок Загрузите значок вашей корзины. Рекомендуемый размер иконки 30x30px WooInstant
 WooInstant - WooCommerce Instant / One Page Checkout
 WooInstant - WooCommerce Instant / One Page Checkout настройки Цвет фона панели корзины WooInstant Цвет фона панели Toogler корзины WooInstant WooInstant Корзина Панель Toggler Цвет текста / значка WooInstant Панель Панель Toggler Текст / Значок Цвет при наведении Цвет фона панели быстрого просмотра WooInstant Настройки WooInstant Wooinstant требует активации WooCommerce да Вы можете отключить его, если у вас уже есть функция добавления ajax в корзину в вашей теме Вы можете отключить его, если у вас уже есть функция быстрого просмотра в вашей теме https://psdtowpservice.com
 https://psdtowpservice.com/wooinstant
 