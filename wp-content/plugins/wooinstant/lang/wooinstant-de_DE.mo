��    :      �      �      �     �  3   �  o    
   s     ~     �     �     �  	   �     �     �     �       %   (     N  �   T  
   �     �     �          &     -     <     U     h     {     �     �  :   �     �  
   �     �     �               2     @     I     [     q     }  =   �  
   �  4   �  =   	  &   K	  .   r	  -   �	  3   �	  ,   
     0
  0   D
     u
  N   y
  H   �
       %   ,  �  R       D     o  ]  
   �     �     �     �          $  "   5     X  !   t  0   �  .   �  
   �  �        �     �     �     �     �        &     #   6     Z     u     �  $   �  P   �               )     .     @     N     i     {     �     �     �     �  _   �  
   O  5   Z  C   �  &   �  .   �  2   *  8   ]  6   �     �  /   �       s     _   �     �  &   	   Active WooInstant Actived window will open first when wooinstant open Are you tired of multi-step checkout process of WooCommerce? WooInstant is your solution. Install WooInstant and your multi-step checkout process will convert to One Page Checkout. Now, all your customer have to do is "Add to Cart", a popup will appear with the cart view. Your customer can then checkout and order from that single window!. No Page Reload whatsoever! BootPeople Cart Cart Toggler Background Cart Toggler Color Cart Toggler Hover Color Cart icon Checkout Choose Active Window Choose Cart Drawer Direction Choose Icon or Custom Image Choose how the window will looks like Close Control WooInstant z-index from this option. More about <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> Custom CSS Custom Image Default Icon Default is: Theme Default Design Design Options Disable Ajax Add to Cart Disable Quick View Drawer Window Type General General Options Hide Close Button? If you want to make extra CSS then you can do it from here Left to Right Multi Step No Panel Background Panel Z-index Quick View Background Right to Left Settings Show On Cart Page Show On Checkout Page Single Step Upload image/icon Upload your cart icon. Recommended size of an icon is 30x30px WooInstant WooInstant - WooCommerce Instant / One Page Checkout WooInstant - WooCommerce Instant / One Page Checkout Settings WooInstant Cart Panel Background Color WooInstant Cart Panel Toggler Background Color WooInstant Cart Panel Toggler Text/Icon Color WooInstant Cart Panel Toggler Text/Icon Hover Color WooInstant Quick View Panel Background Color WooInstant Settings Wooinstant requires WooCommerce to be activated  Yes You can disable it if you already have ajax add to cart function in your theme You can disable it if you already have quick view function in your theme https://psdtowpservice.com https://psdtowpservice.com/wooinstant Project-Id-Version: WooInstant - WooCommerce Instant / One Page Checkout
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-07-09 09:23+0000
PO-Revision-Date: 2020-07-13 11:04+0000
Last-Translator: 
Language-Team: German
Language: de_DE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2 Active WooInstant Das aktivierte Fenster wird zuerst geöffnet, wenn es geöffnet wird Are you tired of multi-step checkout process of WooCommerce? WooInstant is your solution. Install WooInstant and your multi-step checkout process will convert to One Page Checkout. Now, all your customer have to do is "Add to Cart", a popup will appear with the cart view. Your customer can then checkout and order from that single window!. No Page Reload whatsoever! BootPeople Wagen Cart Toggler Hintergrund Wagen Toggler Farbe Cart Toggler Hover Farbe Warenkorb-Symbol Wagen TogCheckout
gler Hover Farbe Wählen Sie Aktives Fenster Wählen Sie Cart Drawer Direction Wählen Sie Symbol oder Benutzerdefiniertes Bild Wählen Sie aus, wie das Fenster aussehen soll Schließen Steuern Sie den WooInstant-Z-Index über diese Option. Mehr über <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> Benutzerdefinierte CSS Benutzerdefiniertes Bild Standardsymbol Standard ist: Theme Default Design Designoptionen Deaktivieren Sie Ajax In den Warenkorb Deaktivieren Sie die Schnellansicht Typ des Schubladenfensters Allgemeines Allgemeine Optionen Schließen-Schaltfläche ausblenden? Wenn Sie zusätzliches CSS erstellen möchten, können Sie dies von hier aus tun Links nach rechts Mehrschritt Nein Panel Hintergrund Panel Z-Index Schnellansicht Hintergrund Rechts nach links Einstellungen Auf Warenkorbseite anzeigen Auf der Checkout-Seite anzeigen Einzelner Schritt Bild / Symbol hochladen Laden Sie Ihr Warenkorbsymbol hoch. Die empfohlene Größe eines Symbols beträgt 30 x 30 Pixel WooInstant WooInstant - WooCommerce Instant / One Page Checkout
 WooInstant - WooCommerce Instant / One Page Checkout Einstellungen
 WooInstant Cart Panel Hintergrundfarbe WooInstant Cart Panel Toggler Hintergrundfarbe WooInstant Cart Panel Toggler Text- / Symbolfarbe
 WooInstant Cart Panel Toggler Text / Symbol Schwebefarbe Hintergrundfarbe des WooInstant Quick View-Bedienfelds WooInstant Einstellungen Für Wooinstant muss WooCommerce aktiviert sein Ja Sie können es deaktivieren, wenn Ihr Thema bereits über eine Ajax-Funktion zum Hinzufügen zum Warenkorb verfügt Sie können es deaktivieren, wenn Ihr Thema bereits über eine Schnellansichtsfunktion verfügt https://psdtowpservice.com
 https://psdtowpservice.com/wooinstant
 