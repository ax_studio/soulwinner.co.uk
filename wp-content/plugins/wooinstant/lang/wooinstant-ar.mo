��    :      �      �      �     �  3   �  o    
   s     ~     �     �     �  	   �     �     �     �       %   (     N  �   T  
   �     �     �          &     -     <     U     h     {     �     �  :   �     �  
   �     �     �               2     @     I     [     q     }  =   �  
   �  4   �  =   	  &   K	  .   r	  -   �	  3   �	  ,   
     0
  0   D
     u
  N   y
  H   �
       %   ,  	  R     \  O   n  O  �               0     I  $   h     �  
   �     �  %   �  (   �  )        I  �   O  *   �     '     ;  I   Y     �     �  =   �  "        /     L     `      v  b   �  %   �           8     =     Q  )   j  %   �     �  ,   �  #   �       "   2  x   U     �  5   �  >     .   O  -   ~  0   �  =   �  B        ^  ,   x     �  g   �  x        �  &   �   Active WooInstant Actived window will open first when wooinstant open Are you tired of multi-step checkout process of WooCommerce? WooInstant is your solution. Install WooInstant and your multi-step checkout process will convert to One Page Checkout. Now, all your customer have to do is "Add to Cart", a popup will appear with the cart view. Your customer can then checkout and order from that single window!. No Page Reload whatsoever! BootPeople Cart Cart Toggler Background Cart Toggler Color Cart Toggler Hover Color Cart icon Checkout Choose Active Window Choose Cart Drawer Direction Choose Icon or Custom Image Choose how the window will looks like Close Control WooInstant z-index from this option. More about <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> Custom CSS Custom Image Default Icon Default is: Theme Default Design Design Options Disable Ajax Add to Cart Disable Quick View Drawer Window Type General General Options Hide Close Button? If you want to make extra CSS then you can do it from here Left to Right Multi Step No Panel Background Panel Z-index Quick View Background Right to Left Settings Show On Cart Page Show On Checkout Page Single Step Upload image/icon Upload your cart icon. Recommended size of an icon is 30x30px WooInstant WooInstant - WooCommerce Instant / One Page Checkout WooInstant - WooCommerce Instant / One Page Checkout Settings WooInstant Cart Panel Background Color WooInstant Cart Panel Toggler Background Color WooInstant Cart Panel Toggler Text/Icon Color WooInstant Cart Panel Toggler Text/Icon Hover Color WooInstant Quick View Panel Background Color WooInstant Settings Wooinstant requires WooCommerce to be activated  Yes You can disable it if you already have ajax add to cart function in your theme You can disable it if you already have quick view function in your theme https://psdtowpservice.com https://psdtowpservice.com/wooinstant Project-Id-Version: WooInstant - WooCommerce Instant / One Page Checkout
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-07-09 09:23+0000
PO-Revision-Date: 2020-07-25 08:01+0000
Last-Translator: 
Language-Team: Arabic
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100 >= 3 && n%100<=10 ? 3 : n%100 >= 11 && n%100<=99 ? 4 : 5;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2 نشط WooInstant سيتم فتح النافذة النشطة أولاً عند فتح wooinstant هل تعبت من عملية الخروج متعددة الخطوات لـ WooCommerce؟ WooInstant هو الحل الخاص بك. قم بتثبيت WooInstant وستتحول عملية الخروج متعددة الخطوات إلى One Page Checkout. الآن ، كل ما يجب على عميلك فعله هو "إضافة إلى سلة التسوق" ، وستظهر نافذة منبثقة مع عرض سلة التسوق. يمكن للعميل بعد ذلك الخروج والطلب من تلك النافذة المفردة !. لا توجد صفحة تحديث على الإطلاق! BootPeople
 عربة التسوق Cart Toggler Background
 عربة تبديل اللون عربة Toggler تحوم اللون رمز سلة التسوق الدفع اختر نافذة نشطة اختر اتجاه درج السلة اختر رمز أو صورة مخصصة اختر كيف ستبدو النافذة Close تحكم في فهرس WooInstant من هذا الخيار. المزيد حول <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/"> z-index </a> لغة تنسيق ويب حسب الطلب صورة مخصصة الرمز الافتراضي الإعداد الافتراضي هو: الموضوع الافتراضي التصميم خيارات التصميم تعطيل اياكس اضافة الى عربة التسوق تعطيل العرض السريع نوع نافذة الدرج جنرال لواء خيارات عامة إخفاء زر الإغلاق؟ إذا كنت تريد إنشاء CSS إضافي ، فيمكنك القيام بذلك من هنا من اليسار إلى اليمين خطوات متعددة لا خلفية لوحة لوحة Z- الفهرس نظرة سريعة على الخلفية من اليمين الى اليسار الإعدادات إظهار في صفحة سلة التسوق عرض على صفحة الخروج خطوة واحدة تحميل صورة / أيقونة قم بتحميل أيقونة سلة التسوق الخاصة بك. الحجم الموصى به لرمز هو 30x30px WooInstant
 WooInstant - WooCommerce Instant / One Page Checkout
 WooInstant - WooCommerce Instant / One Page Checkout Settings
 لون خلفية لوحة عربة WooInstant لون خلفية لوحة WooInstant Toggler WooInstant لوحة Toggler نص / رمز لون WooInstant لوحة Toggler نص / رمز تحوم اللون WooInstant لمحة سريعة عن لوحة خلفية اللون إعدادات WooInstant يتطلب Wooinstant تفعيل WooCommerce نعم يمكنك تعطيله إذا كان لديك بالفعل وظيفة ajax add to cart في مظهرك يمكنك تعطيله إذا كان لديك بالفعل وظيفة عرض سريع في السمة الخاصة بك https://psdtowpservice.com
 https://psdtowpservice.com/wooinstant
 