<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AX_studio
 */

get_header();
?>


    <main class="archive">
        <?php
        while (have_posts()) :
            the_post();

            the_content();


        endwhile;
        ?>
    </main>


<?php
get_footer();
