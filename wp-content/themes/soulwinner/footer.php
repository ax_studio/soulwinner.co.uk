<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>

</div>

<footer class="footer">
<!--    <div id="footer-navigation" class="footer-navigation">-->
<!--        --><?php
//        wp_nav_menu(array(
//            'theme_location' => 'footer',
//            'menu_id' => 'footer-menu',
//        ));
//        ?>
<!--    </div>-->

    <div class="footer-branding">
        <a href="<?php echo site_url(); ?>">
            <img src="<?php echo site_url(); ?>/wp-content/uploads/2020/10/soul-winner-clr.png"/>
        </a>
    </div>
    <div class="footer-copyright">
        <p class="small">© <?php echo date("Y") . ' '. get_bloginfo( 'name' ); ?> — All Rights Reserved • Website By <a href="https://axstudio.uk" target="_blank">AX Studio</a></p>
    </div>
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
