function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function swModal() {
    $('.cd-modal-action .btn').click(function () {
        if($('.modal-is-visible').is(':visible')){
            $('.cd-modal-action .btn').hide();
        }
    });

}

function onScroll() {
    $(window).scroll(function () {

        if ($(this).scrollTop() > 50) {
            $('.header').addClass('change');
            $('.header-scrolldown').fadeOut();
            $('.header-sharethis').fadeOut();
        } else {
            $('.header').removeClass('change')
            $('.header-scrolldown').fadeIn();
            $('.header-sharethis').fadeIn();
        }
    });

}


function scrollSpy() {
    $('.header-navigation ul li a').addClass('nav-link');
    $('.elementor-section-wrap').attr('data-spy', 'scroll');
    $('.elementor-section-wrap').attr('data-target', '#spy');

    var section = document.querySelectorAll(".scrollhere");
    var sections = {};

    Array.prototype.forEach.call(section, function (e) {
        sections[e.id] = e.offsetTop;
    });
}

function frontpageCart() {
    $('.single_add_to_cart_button').click(function () {
        if ($('.added_to_cart').is(':visible')) {
            $('.single_add_to_cart_button').hide();
            $('.front_cart input[type="number"]').hide();
        }
    });
}

function menuToggle() {
    $('.menu-wrapper').click(function () {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $('.header-navigation').fadeIn();
            $('.header-navigation ul li a').click(function () {
                $('.header-navigation').fadeOut();
                $('.menu-wrapper').toggleClass('active');
            });
        } else {
            $('.header-navigation').fadeOut();
        }
    });
}
