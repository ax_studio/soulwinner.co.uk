<?php


function wpshout_excerpt($text)
{
    if (is_admin()) {
        return $text;
    }
    // Fetch the content with filters applied to get <p> tags
    $content = apply_filters('the_content', get_the_content());

    // Stop after the first </p> tag
    $text = substr($content, 0, strpos($content, '</p>') + 3);
    return $text;
}

// Leave priority at default of 10 to allow further filtering
add_filter('wp_trim_excerpt', 'wpshout_excerpt', 10, 1);
