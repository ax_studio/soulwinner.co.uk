<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YZ4X3FY9QS"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YZ4X3FY9QS');
</script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="site transition-fade" id="swup">

    <header id="header" class="header">
        <div class="header-branding">
            <?php the_custom_logo(); ?>

        </div>

        <div id="header-navigation" class="header-navigation">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'primary',
                'menu_id' => 'header-menu',
            ));
            ?>
        </div>
        <div class="header-toggle">
            <div class="menu-wrapper"><span class="menu-cross"></span></div>
        </div>
        <div class="header-right">
            <a>
                <img src="https://soulwinner.co.uk/wp-content/uploads/2020/10/gbl-publishing-logo-90x90-1.png"/>
            </a>
        </div>
        <div class="header-scrolldown">
            <div><p>Scroll Down</p><i class="las la-long-arrow-alt-right"></i></div>
        </div>
        <?php

        if ( is_home() || is_front_page() ) { ?>
            <div class="header-sharethis">
                <p>Share: </p>
                <div>
                    <?php echo do_shortcode( '[elementor-template id="561"]' ); ?>

                </div>
            </div>
        <?php }
        ?>

    </header>

    <div class="site-content">
